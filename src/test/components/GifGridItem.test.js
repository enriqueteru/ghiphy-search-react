import React from 'react';
import { shallow } from 'enzyme';
import {GifGridItem} from '../../components';


describe('Testing GifGridItem', () => {
    const url = 'http://localhost:3000';
    const title = 'hello';
    test('should render the component ok', () => {
        const wrapper = shallow(<GifGridItem title={title} url={url} />)
        expect(wrapper).toMatchSnapshot();
    });
});