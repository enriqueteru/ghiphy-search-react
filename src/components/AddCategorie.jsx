import React from "react";
import { useState } from "react";

function AddCategorie({ handleAdd }) {
  const [inputValue, setInputValue] = useState("");

  const handleInputChange = (ev) => {
    setInputValue(ev.target.value);
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();
    if (inputValue) {
      setInputValue("");
    }
  };

  return (
    <form className="title form" onSubmit={handleSubmit}>
      <input
        className="input"
        type="text"
        value={inputValue}
        onChange={handleInputChange}
      />

      {inputValue.length >= 1 && (
        <button
          className="button blue"
          onClick={() => {
            handleAdd(inputValue);
          }}
     
        >
          Search some gifs
        </button>
      )}

      {inputValue.length < 1 && (
        <button
          className="button grey"
          onClick={() => {
            handleAdd(inputValue);
          }}
          disabled
        >
          Enter a search
        </button>
      )}
    </form>
  );
}

export default AddCategorie;
