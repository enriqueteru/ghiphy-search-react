import React from "react";
import PropTypes from 'prop-types'; //impt


function GifGridItem({ url, title }) {
  return (
    <div className="card animate-fade-in">
      <img className="card__img" src={url} alt={title} />
      <h2 className="card__title">{title}</h2>
    </div>
  );
}


GifGridItem.propTypes = { 
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
}

export default GifGridItem;
