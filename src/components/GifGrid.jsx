import React, { useState, useEffect } from "react";
import { GifGridItem } from './index';
import { useFetchGifs } from "../hooks/useFetchGifs";


function GifGrid({ categorie }) {

const {data:images, loading} = useFetchGifs(categorie);


  return (
    <>
    <h1 className="title">{categorie}</h1>
   { loading === true &&
   <div className="dot"></div>
   } 
<div className="grid">
    
      {images.map((img) => (
        <GifGridItem   key={ img.id } { ...img } />
      ))}
    </div>
    </>
  );
}

export default GifGrid;
