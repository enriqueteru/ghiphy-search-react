export const gifData = async (categorie) => {
    const url =  `https://api.giphy.com/v1/gifs/search?api_key=ZvMdjbDu721oGIPXIsHtdGO5hFWsG2vd&q=${encodeURI( categorie )}&limit=3`;
    const res = await fetch(url);
    const { data } = await res.json();
    const gifs = data.map((img) => {
      return {
        id: img.id,
        title: img.title,
        url: img.images.downsized_medium.url,
      };
    });
    
 return gifs
  }