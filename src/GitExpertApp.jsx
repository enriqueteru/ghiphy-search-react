import React, { useState } from "react";
import { AddCategorie, Footer, GifGrid } from "./components";
import './index.scss'
const GitExpertApp = () => {
  //UseStates
const info = {
  title: 'Gif Searcher App',
  des: 'search 3 random gif ussing the input'
};

  const [categories, setCategories] = useState([]);

  //Fns
  const handleAdd = (item) => (categories.find((el) => el === item) ? setCategories([...categories]) : setCategories((cat) => [item, ...cat ]) )
  const handleReset = ()=>{
  setCategories([])
  
}




  return (
    <div className="container" >
      <h2 style={{margin: '30px 0 '}}>{info.title}</h2>
      <p>{info.des}</p>
      <hr />

      <AddCategorie handleAdd={handleAdd}  />

 
        {

        categories.map(categorie => 
        
        <GifGrid 
        categorie={ categorie } 
        key={ categorie }
        />

        )}

      {categories.length > 0 && 
      
      <button
      className="button red fixed"
      onClick={handleReset}> CLEAR </button>
      
      }

      
     <Footer />
    </div>

  );
};

export default GitExpertApp;
