import { useEffect, useState } from "react";
import { gifData } from "../helpers/gifData";

export const useFetchGifs = (categorie) => {
  const [state, setstate] = useState({
    data: [],
    loading: true,
  });

  useEffect(() => {
    gifData(categorie).then(imgs => setstate(
        {data: [...imgs],
        loading: false}));
  }, [categorie]);

  return state;
};
